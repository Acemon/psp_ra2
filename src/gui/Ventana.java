package gui;

import data.Controller;

import javax.swing.*;

/**
 * Created by Fernando on 14/02/2017.
 */
public class Ventana{
    public JTextArea taConversacion;
    public JTextField tfTexto;
    public JButton btEnviar;
    public JLabel lEscribiendo;
    public JLabel lEstado;
    public JTabbedPane tpUsuarios;
    public JButton btNuevoGrupo;
    public JPanel panel1;
    public JList lOnline;
    public DefaultListModel dlmOnline;
    public JList lOffline;
    public JList lGrupo;
    public JCheckBox chkSilence;
    public JFrame frame;

    public Ventana() {
        frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        dlmOnline = new DefaultListModel();
        lOnline.setModel(dlmOnline);
    }
}
