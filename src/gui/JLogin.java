package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Fernando on 15/02/2017.
 */
public class JLogin extends JDialog implements ActionListener {
    private JPasswordField tfPassword;
    private JTextField tfUsuario;
    private JButton btNuevo;
    private JButton btLogin;
    private JPanel panel;
    private JTextField tfPuerto;
    private Evento evento;
    private String usuario;
    private String pass;
    private String IP;

    public static enum Evento{
        NUEVO, LOGIN;
    }

    public JLogin() {
        setContentPane(panel);
        setModal(true);
        getRootPane().setDefaultButton(btLogin);
        pack();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setLocationRelativeTo(null);
        btLogin.addActionListener(this);
        btNuevo.addActionListener(this);
    }

    public Evento mostrarDialog(){
        setVisible(true);
        return evento;
    }

    public String getPass() {
        return pass;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getIP() {
        return IP;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        usuario = tfUsuario.getText();
        pass = new String(tfPassword.getPassword());
        IP = tfPuerto.getText();
        if (usuario.equals("") || pass.equals("") || IP.equals(""))
            return;
        if (e.getActionCommand().equalsIgnoreCase(btLogin.getActionCommand())){
            evento = Evento.LOGIN;
        }else if (e.getActionCommand().equalsIgnoreCase(btNuevo.getActionCommand())){
            evento = Evento.NUEVO;
        }
        dispose();
    }
}
