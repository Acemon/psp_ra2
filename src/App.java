import data.Controller;
import gui.Ventana;

/**
 * Created by Fernando on 15/02/2017.
 */
public class App {
    public static void main (String[] args){
        Ventana view = new Ventana();
        new Controller(view);
    }
}