package data;

import java.util.ArrayList;

/**
 * Created by Fernando on 17/02/2017.
 */
public class Conversacion {
    private String nombre;
    private boolean ban;
    private ArrayList<String>conver;

    public Conversacion(String nombre) {
        this.nombre = nombre;
        conver = new ArrayList<>();
    }

    public boolean isBan() {
        return ban;
    }

    public void setBan(boolean ban) {
        this.ban = ban;
    }

    public String getNombre() {
        return nombre;
    }

    public ArrayList<String> getConver() {
        return conver;
    }

    public void setConver(ArrayList<String> conver) {
        this.conver = conver;
    }

    public void addConver(String a){
        this.conver.add(a);
    }
}
