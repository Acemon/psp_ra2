package data;

import gui.JLogin;
import gui.Ventana;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import static util.Constantes.*;

/**
 * Created by Fernando on 15/02/2017.
 */
public class Controller implements ActionListener, ListSelectionListener, WindowListener, ChangeListener {
    private String IP;
    Ventana view;
    Socket socketCliente;
    PrintWriter salida;
    BufferedReader entrada;
    String hablando;
    String username;
    String pass;
    boolean conexion;
    boolean inicio;
    JLogin jLogin;
    ArrayList<Conversacion> conversaciones;

    public Controller(Ventana view) {
        this.view = view;
        conversaciones = new ArrayList<>();
        view.lOnline.addListSelectionListener(this);
        view.btEnviar.addActionListener(this);
        view.btNuevoGrupo.addActionListener(this);
        view.chkSilence.addChangeListener(this);
        view.frame.addWindowListener(this);
        conexion = false;
        loguear();
    }

    private void conectar() {
        while (!conexion){
            try {
                conectarServidor();
                conexion=true;
                view.lEstado.setText("Conectado satisfactoriamente al servidor");
            } catch (IOException e) {
                view.lEstado.setText("Error al sincronizar con el servidor");
                loguear();
            }
        }
        inicio = false;
    }

    private void conectarServidor() throws IOException {
        socketCliente = new Socket(IP, PUERTO);
        salida = new PrintWriter(socketCliente.getOutputStream(), true);
        entrada = new BufferedReader(new InputStreamReader(socketCliente.getInputStream()));

        Tarea tarea = new Tarea(socketCliente, entrada);
        tarea.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equalsIgnoreCase("mensaje")){
                    String[] cadena = (String[]) evt.getNewValue();
                    if (cadena[1].equalsIgnoreCase("servidor")){
                        view.taConversacion.append(cadena[1]+": "+cadena[2]+"\n");
                    }else{
                        Conversacion con =buscar(conversaciones, cadena[1]);
                        con.addConver(cadena[2]);
                        if (hablando.equals(con.getNombre()) && !con.isBan()){
                            refrescarta(con);
                        }
                    }
                }
                else if (evt.getPropertyName().equalsIgnoreCase("nicks")){
                    String [] nicks = (String[]) evt.getNewValue();
                    view.dlmOnline.removeAllElements();
                    for (int i =1; i<nicks.length; i++){
                        view.dlmOnline.addElement(nicks[i]);
                        if (!existe(conversaciones,nicks[i])){
                            Conversacion conver = new Conversacion(nicks[i]);
                            conversaciones.add(conver);
                        }
                    }
                    view.dlmOnline.removeElement(username);
                }
                else if (evt.getPropertyName().equalsIgnoreCase("login")){
                    String[] login = (String[]) evt.getNewValue();
                    inicio=Boolean.parseBoolean(login[1]);
                    if (!inicio){
                        loguear();
                    }else{
                        view.lEscribiendo.setText("Usuario: "+hablando);
                    }
                }
            }
        });
        tarea.execute();
    }

    private void loguear() {
        jLogin = new JLogin();
        JLogin.Evento evento = jLogin.mostrarDialog();
        username = jLogin.getUsuario();
        pass = jLogin.getPass();
        IP = jLogin.getIP();
        hablando = username;
        conectar();
        if (!username.equals("") && !pass.equals("")){
            if (evento == JLogin.Evento.LOGIN){
                iniciarSesion(username, pass);

            } else if (evento == JLogin.Evento.NUEVO){
                registrar(username, pass);
            }
        }
    }

    private void iniciarSesion(String username, String pass) {
        salida.println("login#"+username+"#"+pass);
    }

    private void registrar(String username, String pass) {
        salida.println("new#"+username+"#"+pass);
    }

    private Conversacion buscar(ArrayList<Conversacion> conversaciones, String s) {
        for (Conversacion con : conversaciones){
            if (con.getNombre().equalsIgnoreCase(s)){
                return con;
            }
        }
        return null;
    }

    private boolean existe(ArrayList<Conversacion> conversaciones, String nick) {
        for (int i =0; i<conversaciones.size(); i++){
            if (conversaciones.get(i).getNombre().equalsIgnoreCase(nick)){
                return true;
            }
        }
        return false;
    }

    private void enviarMensaje(){
        //TODO
        String mensaje = view.tfTexto.getText();
        if (mensaje.equals(""))
            return;
        view.tfTexto.setText("");
        salida.println("/user#"+hablando+"#"+mensaje);
        if (!hablando.equalsIgnoreCase(username)){
            Conversacion con = buscar(conversaciones, hablando);
            con.getConver().add(">"+mensaje);
            anadirConversacion(hablando);
        }
    }

    private void anadirConversacion(String hablando) {
        Conversacion con = buscar(conversaciones, hablando);
        view.chkSilence.setSelected(con.isBan());
        if (!con.isBan()){
            view.taConversacion.setText("");
            for (int i =0; i<con.getConver().size(); i++) {
                view.taConversacion.append(con.getConver().get(i)+"\n");
            }
        }
    }

    private void refrescarta(Conversacion con) {
        view.taConversacion.setText("");
        for (int i = 0; i<con.getConver().size(); i++){
            view.taConversacion.append(con.getConver().get(i)+"\n");
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().equalsIgnoreCase(view.btEnviar.getActionCommand())){
            enviarMensaje();
        }else if (ae.getActionCommand().equalsIgnoreCase(view.btNuevoGrupo.getActionCommand())){
            //todo sistema de grupos
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {
        if (lse.getSource().equals(view.lOnline)){
            String valor =  (String) view.lOnline.getSelectedValue();
            if (valor==null){
                hablando = username;
            }
            else{
                hablando = valor;
                anadirConversacion(hablando);
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        salida.println("/exit");
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {
        if (e.getSource().equals(view.chkSilence)){
            Conversacion con = buscar(conversaciones, hablando);
            con.setBan(view.chkSilence.isSelected());
            System.out.println(con.getNombre()+" "+con.isBan());
        }
    }
}
