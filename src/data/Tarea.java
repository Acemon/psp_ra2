package data;

import javax.swing.*;
import java.io.BufferedReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Fernando on 15/02/2017.
 */
public class Tarea extends SwingWorker<Void, Void> {
    private Socket socketCliente;
    private BufferedReader entrada;

    public Tarea(Socket socketCliente, BufferedReader entrada) {
        this.socketCliente = socketCliente;
        this.entrada = entrada;
    }

    @Override
    protected Void doInBackground() throws Exception {
        while (socketCliente.isConnected()){
            String mensaje = entrada.readLine();
            if (mensaje != null){
                if (mensaje.startsWith("/")){
                    String[] partes = mensaje.split("#");
                    switch (partes[0]){
                        case "/nicks":
                            firePropertyChange("nicks", null, partes);
                            break;
                        case "/login":
                            firePropertyChange("login", null, partes);
                            break;
                        case "/msn":
                            firePropertyChange("mensaje", null, partes);
                    }
                }
                    //firePropertyChange("mensaje", null, mensaje);
            }
        }
        return null;
    }
}
